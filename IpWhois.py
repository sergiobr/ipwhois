# WHOIS sobre IP
# 5-11-19
# Sergio Bastian Rodriguez
# https://www.tiraquelibras.com/blog/?p=714

# Importamos las librerias necesarias
import sys, pprint
from ipwhois import IPWhois
from IPy import IP

# Obtenemos el argumento indicado al ejecutar la herramienta
ip = sys.argv[1]

# Logica de la heramienta
try:
        # Confirmamos si el argumento es una direccion IP
	ipType = IP(ip)

        # Confirmamos si la direccion IP es publica o no
	if ipType.iptype() == 'PUBLIC':

                # De ser publica obtenemos los datos del whois y los mostramos
		obj = IPWhois(ip)
		results = obj.lookup_whois()
		pprint.pprint(results)
	else:
                # De no ser publica lo indicamos en un mensaje de error
		print('The IP ' + ip + ' is not public.')

except Exception as e:
        # Si el argumento no es una dirección IP valida, o no es publica, mostramos el mensaje con el error
	print('ERROR - The argument added is not correct.\n%s' % (str(e)))

finally:
        # Si el argumento no es una dirección IP valida, o no es publica, mostramos el mensaje con el error
	print('\n***Herramienta desarrollada por:\n Tiraquelibras.com (https://www.tiraquelibras.com/blog/?p=714)***')
