# Intro

Programa para la obtención de los datos de registro de una IP, escrito en Python en un entorno virtual.
También se agrega un script **Bash** para ejeuctar desde una **CLI** de **Linux**.

# Entorno virtual

Podemos usar el programa desde un entorno virtual de **Python**.
El entorno virtual lo lamaremos ***whois_python***.

```python
python3 -m virtualenv whois_python
```

# Paquetes necesarios

Debemos de instalar los siguientes paquetes, los cuales se encuentran en el archivo ***requirements.txt***:

- pprint (0.1)
- ipwhois (1.1.0)
- IPy (1.00)

Para instalar estos desde el archivo indicado ejecutamos el siguiente comando:

```bash
pip install -r requirements.txt
```
Los podemos instalarlos en nuestro sistema local o en el entorno virtual comentado en el apartado anterior.

# Script Bash

El archivo ***ipwhois*** lo ubicamos en el directorio ***/bin*** con los siguientes permisos:

```bash
-rwxr-xr-x    1 root root         436 nov  5 12:13 ipwhois
```

De esta forma cualquier usuario del sistema lo podrá ejecutar.


# Ejecución

Para ejecutarlo podemos hacerlo de las siguientes formas:

## Desde Python

Podemo ejecutarlo desde Python, tanto si instalamos un entornor virtual como si lo hicimos directamente en el sistema.

### Entorno virtual

Activamos el entorno virtual:

```python
source whois_python/bin/activate
```

Y ejecutamos el comando:

```python
python3 IpWhois.py <dir_IP>
```

### Sin entorno virtual

Ejecutamos el comando:

```python
python3 IpWhois.py <dir_IP>
```

## Desde el script Bash personalizado

Si instalamos el comando personalizado para ejeuctar desde la **CLI** de **Linux** ejecutamos el comando de la siguiente manera:

```bash
ipwhois <dir_IP>
```

Disponemos de una ayuda para el comando:

```bash
# ipwhois --help
Whois of IP address.
Only one IP by command.
ipwhois [-h|--help]
```

# Enlace

En el siguiente enlace se publica la información de este programa, https://www.tiraquelibras.com/blog/?p=714.